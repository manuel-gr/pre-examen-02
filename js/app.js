document.addEventListener('DOMContentLoaded', function () {
    const razasSelect = document.getElementById('razas');
    const cargarRazaButton = document.getElementById('cargarRazaButton');
    const verImagenButton = document.getElementById('verImagenButton');
    const imagenPerro = document.getElementById('imagenPerro');

    // Función para cargar las razas en el combobox
    async function cargarRazas() {
        try {
            const response = await fetch('https://dog.ceo/api/breeds/list');
            const data = await response.json();
            const razas = data.message;

            // Agregar la opción predeterminada
            const defaultOption = document.createElement('option');
            defaultOption.value = '';
            defaultOption.textContent = 'Razas Caninas';
            defaultOption.selected = true;
            razasSelect.appendChild(defaultOption);

            razas.forEach(raza => {
                const option = document.createElement('option');
                option.value = raza;
                option.textContent = raza;
                razasSelect.appendChild(option);
            });
        } catch (error) {
            console.error('Error al cargar las razas:', error);
        }
    }

    // Función para cargar una imagen de la raza seleccionada
    async function cargarImagenRaza() {
        const razaSeleccionada = razasSelect.value;
        try {
            const response = await fetch(`https://dog.ceo/api/breed/${razaSeleccionada}/images/random`);
            const data = await response.json();
            const imageUrl = data.message;

            imagenPerro.src = imageUrl;
        } catch (error) {
            console.error('Error al cargar la imagen:', error);
        }
    }

    // Evento para cargar las razas al cargar la página
    cargarRazas();

    // Evento para habilitar el botón "Mostrar Imagen" al cargar una raza
    cargarRazaButton.addEventListener('click', function () {
        verImagenButton.disabled = false;
    });

    // Evento para mostrar una imagen al presionar el botón "Mostrar Imagen"
    verImagenButton.addEventListener('click', cargarImagenRaza);
});
